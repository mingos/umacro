<?php
namespace Mingos\uMacro\ParamParser;

use Mingos\uMacro\ParamParserInterface;

/**
 * Params parser that parses parametres using HTML attributes syntax.
 */
class XMLAttributes implements ParamParserInterface
{
	/**
	 * @inheritdoc
	 */
	public function parse($paramsString)
	{
		$element = new \SimpleXMLElement("<element {$paramsString}></element>");
		$attributes = (array) $element->attributes();

		return $attributes["@attributes"];
	}
}
