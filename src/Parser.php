<?php
namespace Mingos\uMacro;

/**
 * Macro parser class.
 */
class Parser
{
	/**
	 * String used for separating the macro name from its parametres
	 * @var string
	 */
	private $separator;

	/**
	 * Quoted version of the separator
	 * @var string
	 */
	private $separatorQuoted;

	/**
	 * Macro's opening delimiter
	 * @var string
	 */
	private $start;

	/**
	 * Quoted version of the opening delimiter
	 * @var string
	 */
	private $startQuoted;

	/**
	 * Macro's closing delimiter
	 * @var string
	 */
	private $end;

	/**
	 * Quoted version of the closing delimiter
	 * @var string
	 */
	private $endQuoted;

	/**
	 * Custom macro name -> class name mappings
	 * @var array
	 */
	private $map = array();

	/**
	 * @var ParamParserInterface
	 */
	private $paramParser;

	/**
	 * Create a parser instance
	 *
	 * @param ParamParserInterface $paramParser An object implementing ParamParserInterface; it will be injected into
	 *                                          each instantiated macro.
	 * @param string               $start       String identifying the start of a macro, as part of a regular expression
	 * @param string               $separator   String used for separating the macro name from its parametres, as part
	 *                                          of a regular expression
	 * @param string               $end         String identifying the end of a macro, as part of a regular expression
	 */
	public function __construct(ParamParserInterface $paramParser, $start = "{{{", $separator = "?", $end = "}}}")
	{
		$this->paramParser = $paramParser;
		$this->start = $start;
		$this->separator = $separator;
		$this->end = $end;

		$this->startQuoted = preg_quote($start, "/");
		$this->separatorQuoted = preg_quote($separator, "/");
		$this->endQuoted = preg_quote($end, "/");
	}

	/**
	 * Replace the macros in an input string
	 *
	 * @param  string $input
	 * @return string
	 */
	public function replace($input)
	{
		$ex = "/{$this->startQuoted}(?P<name>[A-Za-z0-9_]+)({$this->separatorQuoted}(?P<params>.+))?{$this->endQuoted}/U";
		return preg_replace_callback($ex, array($this, "_replace"), $input);
	}

	/**
	 * Run the replacement code on a given macro string
	 *
	 * @param  array                     $macro
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	private function _replace($macro)
	{
		// extract macro name
		$name = $macro["name"];

		// extract macro params, if any
		$params = array_key_exists("params", $macro) ? $macro["params"] : "";

		if (array_key_exists($name, $this->map)) {
			$class = $this->map[$name];
		} else {
			$macroName = mb_convert_case($name, MB_CASE_TITLE, "utf-8");
			$class = "\\Mingos\\uMacro\\Macro\\{$macroName}";
		}

		if (!class_exists($class)) {
			throw new \InvalidArgumentException("Unrecognised macro \"{$name}\".", 500);
		}

		/**
		 * @var Macro $macro
		 */
		$macro = new $class($this->paramParser, $params);

		return $macro->run();
	}

	/**
	 * Add a name to class mapping
	 *
	 * @param string $name
	 * @param string $class
	 * @return Parser provides a fluent interface
	 */
	public function addMap($name, $class)
	{
		$this->map[$name] = $class;

		return $this;
	}
}
