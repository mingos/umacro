<?php
namespace Mingos\uMacro\Macro;

use Mingos\uMacro\Macro;

/**
 * Macro class for SoundCloud songs
 */
class Soundcloud extends Macro
{
	/**
	 * Configure the expected parametres
	 */
	public function configParams()
	{
		$this->addParam("id");
		$this->addParam("width", "100%");
		$this->addParam("height", "166");
		$this->addParam("auto_play", "false");
		$this->addParam("show_artwork", "true");
		$this->addParam("color", "ff7700");
	}

	/**
	 * Replace a Soundcloud macro
	 *
	 * @return string
	 */
	public function run()
	{
		$params = $this->getParams();

		return <<<EOS
<iframe
	width="{$params["width"]}"
	height="{$params["height"]}"
	scrolling="no"
	frameborder="no"
	src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F{$params["id"]}&amp;auto_play={$params["auto_play"]}&amp;show_artwork={$params["show_artwork"]}&amp;color={$params["color"]}"
	></iframe>
EOS;
	}
}
