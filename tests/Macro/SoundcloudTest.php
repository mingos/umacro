<?php
namespace Mingos\uMacro\Macro;

use Mingos\uMacro\ParamParser\ParseStr;

/**
 * Unit tests for the class uMacro\Macro\Soundcloud
 */
class SoundcloudTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Default values should be applied when not specified
	 */
	public function testDefaults()
	{
		$expectedOutput = <<<EOS
<iframe
	width="100%"
	height="166"
	scrolling="no"
	frameborder="no"
	src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2Flumberjack&amp;auto_play=false&amp;show_artwork=true&amp;color=ff7700"
	></iframe>
EOS;
		$macro = new Soundcloud(new ParseStr(), "id=lumberjack");

		$this->assertEquals($expectedOutput, $macro->run());
	}

	/**
	 * When specified, params should be used, discarding default values
	 */
	public function testAllParams()
	{
		$expectedOutput = <<<EOS
<iframe
	width="320"
	height="240"
	scrolling="no"
	frameborder="no"
	src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2Flumberjack&amp;auto_play=true&amp;show_artwork=false&amp;color=0077ff"
	></iframe>
EOS;
		$macro = new Soundcloud(new ParseStr(), "id=lumberjack&width=320&height=240&auto_play=true&show_artwork=false&color=0077ff");

		$this->assertEquals($expectedOutput, $macro->run());
	}

	/**
	 * Missing ID should throw an exception
	 */
	public function testMissingId()
	{
		$this->setExpectedException("\\InvalidArgumentException");
		new Soundcloud(new ParseStr(), "");
	}
}
