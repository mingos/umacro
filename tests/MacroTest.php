<?php
namespace Mingos\uMacro;
use Mingos\uMacro\ParamParser\ParseStr;
use Mingos\uMacro\ParamParser\XMLAttributes;

/**
 * Fake macro for testing purposes
 */
class Grail extends Macro
{
	public function configParams()
	{
		$this->addParam("name", "Arthur");
		$this->addParam("occupation", "king");
		$this->addParam("quest", "seek the Holy Grail");
	}

	public function run()
	{
		$params = $this->getParams();
		$colour = $this->getParam("colour");
		return "{$params["occupation"]} {$params["name"]}, {$params["quest"]}"
			. (null === $colour ? "" : ", {$colour}");
	}
}

/**
 * Another fake macro for testing purposes
 */
class Knights extends Macro
{
	public function configParams()
	{
		$this->addParam("say");
	}

	public function run()
	{
		$say = $this->getParam("say");
		return "We are the knights who say '{$say}'.";
	}
}

/**
 * Unit tests for the class uMacro\Macro
 */
class MacroTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Parser
	 */
	private $parser;

	public function setUp()
	{
		$this->parser = new Parser(new ParseStr());
		$this->parser
			->addMap("grail", "\\Mingos\\uMacro\\Grail")
			->addMap("knights", "\\Mingos\\uMacro\\Knights")
		;
	}

	/**
	 * A macro can run with no params, if none are required
	 */
	public function testDefaultParams()
	{
		$output = $this->parser->replace("{{{grail}}}");
		$this->assertEquals("king Arthur, seek the Holy Grail", $output);
	}

	/**
	 * Specifying all params should render the macro with all of them, replacing any default values
	 */
	public function testAllParams()
	{
		$output = $this->parser->replace("{{{knights?say=ni}}}");
		$this->assertEquals("We are the knights who say 'ni'.", $output);

		$output = $this->parser->replace("{{{grail?name=Putey&occupation=lumberjack"
			. "&quest=hang around in bars&colour=blue&phony=fake}}}");
		$this->assertEquals("lumberjack Putey, hang around in bars, blue", $output);
	}

	/**
	 * The param order should be irrelevant
	 */
	public function testParamsInAnyOrder()
	{
		$output = $this->parser->replace("{{{grail?occupation=lumberjack&quest=hang around in bars&name=Putey}}}");
		$this->assertEquals("lumberjack Putey, hang around in bars", $output);
	}

	/**
	 * Specifying only some of the possible parametres should render the macro with defaults in place of the missing
	 * parametres.
	 */
	public function testNotAllParams()
	{
		$output = $this->parser->replace("{{{grail?occupation=lumberjack}}}");
		$this->assertEquals("lumberjack Arthur, seek the Holy Grail", $output);
	}

	/**
	 * If a parametre has no default value, throw an exception
	 */
	public function testRequiredParam()
	{
		$this->setExpectedException('\InvalidArgumentException', 'The parametre "say" is required.', 500);
		$this->parser->replace("{{{knights}}}");
	}

	/**
	 * Check if passing in a different param parser works
	 */
	public function testAlternativeParamParser()
	{
		$this->parser = new Parser(new XMLAttributes(), "{{{", " ", "}}}");
		$this->parser
			->addMap("grail", "\\Mingos\\uMacro\\Grail")
			->addMap("knights", "\\Mingos\\uMacro\\Knights")
		;

		$output = $this->parser->replace('{{{knights say="ni"}}}');
		$this->assertEquals("We are the knights who say 'ni'.", $output);

		$output = $this->parser->replace('{{{grail name="Putey" occupation="lumberjack" quest="hang around in bars"}}}');
		$this->assertEquals("lumberjack Putey, hang around in bars", $output);
	}
}
