<?php
namespace Mingos\uMacro\Macro;

use Mingos\uMacro\ParamParser\ParseStr;

/**
 * Unit tests for the class uMacro\Macro\Vimeo
 */
class VimeoTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Default values should be applied when not specified
	 */
	public function testDefaults()
	{
		$expectedOutput = <<<EOS
<iframe
	width="560"
	height="315"
	src="http://player.vimeo.com/video/lumberjack"
	frameborder="0"
	allowfullscreen
	></iframe>
EOS;
		$macro = new Vimeo(new ParseStr(), "id=lumberjack");

		$this->assertEquals($expectedOutput, $macro->run());
	}

	/**
	 * When specified, params should be used, discarding default values
	 */
	public function testAllParams()
	{
		$expectedOutput = <<<EOS
<iframe
	width="320"
	height="240"
	src="http://player.vimeo.com/video/lumberjack"
	frameborder="0"
	allowfullscreen
	></iframe>
EOS;
		$macro = new Vimeo(new ParseStr(), "id=lumberjack&width=320&height=240");

		$this->assertEquals($expectedOutput, $macro->run());
	}

	/**
	 * Missing ID should throw an exception
	 */
	public function testMissingId()
	{
		$this->setExpectedException("\\InvalidArgumentException");
		new Vimeo(new ParseStr(), "");
	}
}
