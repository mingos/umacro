<?php
namespace Mingos\uMacro\Macro;

use Mingos\uMacro\Macro;

/**
 * Macro class for YouTube macros
 */
class Youtube extends Macro
{
	/**
	 * Configure the expected parametres
	 */
	public function configParams()
	{
		$this->addParam("id");
		$this->addParam("width", "560");
		$this->addParam("height", "315");
	}

	/**
	 * Replace a YouTube macro
	 *
	 * @return string
	 */
	public function run()
	{
		$params = $this->getParams();

		return <<<EOS
<iframe
	width="{$params["width"]}"
	height="{$params["height"]}"
	src="http://www.youtube.com/embed/{$params["id"]}"
	frameborder="0"
	allowfullscreen
	></iframe>
EOS;
	}
}
