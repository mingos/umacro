<?php
namespace Mingos\uMacro;

/**
 * Unit tests for the class uMacro\Version
 */
class VersionTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * The version string must be assembled correctly
	 */
	public function testVersionOutput()
	{
		$maj = Version::MAJOR;
		$min = Version::MINOR;
		$pat = Version::PATCH;

		$output = "{$maj}.{$min}.{$pat}";

		$this->assertEquals($output, Version::versionString());
	}
}
