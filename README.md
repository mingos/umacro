# uMacro

uMacro is a PHP tool used to parse and replace macros contained within a string of text.

## Usage

Creating a macro parser is straightforward: just create an instance of the `Parser` class and pass it an object that
implements the `ParamParserInterface`:

```php
<?php
use Mingos\uMacro\Parser;
use Mingos\uMacro\ParamParser\ParseStr;

$parser = new Parser(new ParseStr());
```

From this point onwards, any string of text can pe passed through the `Parser#replace()` method in order to have the
macros replaced.

```php
<?php
$string = "Watch this video: {{{youtube?id=ykwqXuMPsoc}}}";
echo $parser->replace($string);
```

The macro is parsed whenever a string matching the macro pattern is found. The default pattern using the above config is
`{{{macro_name?query_params}}}`. In other words, macros are surrounded by triple braces and are comprised of the macro
name and parametres. The macro name is separated from the parametres by a question mark. The parametres have the form of
a query string, i.e. a string parseable by `parse_str()`.

Note that this config can be changed.

## Available parametre parsers

There are two available parametre parsers that can be injected into the `Macro` instance. They influence how the
parametre strings are parsed and allow for different syntaxes.

### `Mingos\uMacro\ParamParser\ParseStr`

Accepts parametres formatted as a query string, e.g. `{{{youtube?id=ykwqXuMPsoc&width=560}}}`

### `Mingos\uMacro\ParamParser\XMLAttributes`

Accepts parametres formatted as XML attributes, e.g. `[youtube id="ykwqXuMPsoc" width="560"]`.

Note that the above example shows non-default settings for the macro delimiters and separator (see the section
regarding macro customisation).

## Creating own parametre parsers

It's possible to create a new parametre parser in case a different syntax is needed for the params. The parser class
must implement the `Mingos\uMacro\ParamParserInterface` interface. The interface enforces the implementation of the
`parse()` method. The method receives the parametres as a raw string, exactly as they appeared in the text macro. The
output is always an associative array of parametres.

```php
<?php
namespace My\App;

use Mingos\uMacro\ParamParserInterface;

class MyParser implements ParamParserInterface
{
    public function parse($paramsString)
    {
        $output = [];
        // extract param names and values from $paramsString

        return $output;
    }
}
```

To use the new parametres parser, just inject the newly created object in the `Parser` constructor:

```php
<?php
$parser = new Parser(new MyParser());
```

## Customise the macros

The `Parser` class constructor accepts three optional arguments: opening macro delimiter, macro/parametres separator,
closing macro delimiter. So if the default settings are not acceptable in a given project, the new ones should be passed
to the `Parser` constructor:

```php
<?php
$parser = new Parser(new XMLAttributes(), "[", " ", "]");
```

The strings parsed with such a parser will react only to the defined macro syntax:

```php
<?php
$string = 'Watch this video: [youtube id="ykwqXuMPsoc" width="560"]';
echo $parser->replace($string);
```

## Available macros

There are a few predefined macros available.

### Youtube

Creates a YouTube iframe.

* name: `youtube`
* params:
    * `id` (required)
    * `width` (optional, default: 560)
    * `height` (optional, default: 315)
* example: `{{{youtube?id=ykwqXuMPsoc&width=320&height=240}}}`

### Vimeo

Creates a Vimeo iframe.

* name: `youtube`
* params:
    * `id` (required)
    * `width` (optional, default: 560)
    * `height` (optional, default: 315)
* example: `{{{vimeo?id=17798681&width=320&height=240}}}`

### Soundclound

Creates a Soundcloud iframe.

* name: `soundcloud`
* params:
    * `id` (required)
    * `width` (optional, default: 100%)
    * `height` (optional, default: 166)
    * `auto_play` (optional, default: false)
    * `show_artwork` (optional, default: true)
    * `color` (optional, default: ff7700)
* example: `{{{soundcloud?id=62438266&width=80%&height=200}}}`

### Creating custom macros

Each new macro needs to extend the `Macro` abstract class:

```php
<?php
namespace My\App;

use Mingos\uMacro\Macro;

class MyMacro extends Macro
{
    public function configParams()
    {
        // ...
    }
    public function run()
    {
        // ...
    }
}
```

The macro must implement two methods: `configParams()` and `run()`. The first one tells the macro what parametres to
look for and whether they have default values (i.e. can be omitted). The param configuration is done using the
`addParam()` method:

```php
<?php
public function configParams()
{
    $this->addParam("id"); // mandatory
    $this->addParam("width", "560"); // optional, with
                                     // default value
}
```

If a parametre is optional, but has no default value (i.e. can be omitted completely, but will be taken into accunt if
provided in the macro string), it needn't be defined in the `configParams()` method at all, but should be checked in the
`run()` method.

The `run()` method builds the macro output and returns it. It can use the methods `getParam()` and `getParams()` to
fetch the parametres passed in to the macro.

All provided parametres will be returned by `getParams` - ones passed in by the user to the macro string and the ones
that have a default value in the config. However, omittable params that have been omitted will not be present. They
should be checked explicitly with `getParam()`, which will return `null` if a param is not provided.

```php
<?php
public function run()
{
    $id = $this->getParam("id");
    $params = $this->getParams();
    $optional = $this->getParam("optional");
    return "<output>{$id} {$params['width']}</output>" .
        (null === $optional ? "" : "Optional provided!");
}
```

Once the macro is created, a mapping for it should be added so that it's recognised by the parser:

```php
<?php
$parser->addMap("my_macro", 'My\App\MyMacro');
```