<?php
namespace Mingos\uMacro\ParamParser;

/**
 * Unit test for the parse_str param parser
 */
class ParseStrTest extends \PHPUnit_Framework_TestCase
{
	public function testParser()
	{
		$parser = new ParseStr();

		$this->assertSame(
			["foo" => "bar", "bat" => "qux", "arr" => ["1", "2"]],
			$parser->parse("foo=bar&bat=qux&arr[]=1&arr[]=2")
		);
	}
}
