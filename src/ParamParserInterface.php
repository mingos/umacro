<?php
namespace Mingos\uMacro;

/**
 * Interface for different macro parametres parsers
 */
interface ParamParserInterface
{
	/**
	 * Parse the params of a macro
	 *
	 * @param  string $paramsString
	 * @return array
	 */
	public function parse($paramsString);
}
