<?php
namespace Mingos\uMacro;

/**
 * Base class for all macros
 */
abstract class Macro
{
	/**
	 * Macro parametres
	 * @var array
	 */
	private $params = array();

	/**
	 * Retrieve parametres from the passed string
	 *
	 * @param ParamParserInterface $paramParser
	 * @param string               $params
	 */
	public function __construct(ParamParserInterface $paramParser, $params)
	{
		// configure the parametres a macro expects to receive
		$this->configParams();

		// parse the input parametres
		$params = $paramParser->parse($params);
		foreach ($params as $name => $value) {
			$this->setParam($name, $value);
		}

		// make sure there are no undefined params
		$this->checkParams();
	}

	/**
	 * Run the macro replacement
	 *
	 * @return string
	 */
	abstract function run();

	/**
	 * Macro parametre configuration
	 */
	abstract function configParams();

	/**
	 * Add a parametre
	 *
	 * @param  string                    $name
	 * @param  string|null               $default
	 */
	public function addParam($name, $default = null)
	{
		$this->params[$name] = array_key_exists($name, $this->params) ? $this->params[$name] : $default;
	}

	/**
	 * Set a parametre value.
	 *
	 * @param string $name
	 * @param string $value
	 */
	public function setParam($name, $value)
	{
		$this->params[$name] = $value;
	}

	/**
	 * Get a parametre value
	 *
	 * @param  string      $name
	 * @return string|null
	 */
	public function getParam($name)
	{
		if (array_key_exists($name, $this->params)) {
			return $this->params[$name];
		}

		return null;
	}

	/**
	 * Fetch all parametres
	 *
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * Check whether all params have values.
	 *
	 * @throws \InvalidArgumentException
	 */
	private function checkParams()
	{
		foreach ($this->params as $name => $value) {
			if (null === $value) {
				throw new \InvalidArgumentException("The parametre \"{$name}\" is required.", 500);
			}
		}
	}
}
