<?php
namespace Mingos\uMacro\ParamParser;

/**
 * Unit test for the XML attributes param parser
 */
class XMLAttributesTest extends \PHPUnit_Framework_TestCase
{
	public function testParser()
	{
		$parser = new XMLAttributes();

		$this->assertSame(
			["foo" => "bar", "bat" => "qux"],
			$parser->parse('foo="bar" bat="qux"')
		);
	}
}
