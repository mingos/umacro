<?php
namespace Mingos\uMacro\Macro;

use Mingos\uMacro\ParamParser\ParseStr;

/**
 * Unit tests for the class uMacro\Macro\Youtube
 */
class YoutubeTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Default values should be applied when not specified
	 */
	public function testDefaults()
	{
		$expectedOutput = <<<EOS
<iframe
	width="560"
	height="315"
	src="http://www.youtube.com/embed/lumberjack"
	frameborder="0"
	allowfullscreen
	></iframe>
EOS;
		$macro = new Youtube(new ParseStr(), "id=lumberjack");

		$this->assertEquals($expectedOutput, $macro->run());
	}

	/**
	 * When specified, params should be used, discarding default values
	 */
	public function testAllParams()
	{
		$expectedOutput = <<<EOS
<iframe
	width="320"
	height="240"
	src="http://www.youtube.com/embed/lumberjack"
	frameborder="0"
	allowfullscreen
	></iframe>
EOS;
		$macro = new Youtube(new ParseStr(), "id=lumberjack&width=320&height=240");

		$this->assertEquals($expectedOutput, $macro->run());
	}

	/**
	 * Missing ID should throw an exception
	 */
	public function testMissingId()
	{
		$this->setExpectedException("\\InvalidArgumentException");
		new Youtube(new ParseStr(), "");
	}
}
