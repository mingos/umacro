<?php
namespace Mingos\uMacro;
use Mingos\uMacro\ParamParser\ParseStr;

/**
 * Unit tests for the class uMacro\Parser
 */
class ParserTest extends \PHPUnit_Framework_TestCase
{
	private $expectedOutput = <<<EOS
Hey, watch this <iframe
	width="560"
	height="315"
	src="http://www.youtube.com/embed/lumberjack"
	frameborder="0"
	allowfullscreen
	></iframe> video. And this <iframe
	width="560"
	height="315"
	src="http://player.vimeo.com/video/lumberjack"
	frameborder="0"
	allowfullscreen
	></iframe> video too!
EOS;

	/**
	 * Replacement strings must take place of the macros
	 */
	public function testDefaultReplacement()
	{
		$parser = new Parser(new ParseStr());

		$input = "Hey, watch this {{{youtube?id=lumberjack}}} video. And this {{{vimeo?id=lumberjack}}} video too!";
		$output = $parser->replace($input);

		$this->assertEquals($this->expectedOutput, $output);
	}

	/**
	 * Macro delimiters & separators must be configurable
	 */
	public function testReplacementWithCustomDelimiters()
	{
		$parser = new Parser(new ParseStr(), "<macro>", "#", "</macro>");

		$input = "Hey, watch this <macro>youtube#id=lumberjack</macro> video. And this <macro>vimeo#id=lumberjack</macro> video too!";
		$output = $parser->replace($input);

		$this->assertEquals($this->expectedOutput, $output);
	}

	/**
	 * It must be possible to use custom mappings for macro names
	 */
	public function testReplacementWithCustomMapping()
	{
		$parser = new Parser(new ParseStr());

		$parser
			->addMap("iSleepAllNight", '\Mingos\uMacro\Macro\Youtube')
			->addMap("iWorkAllDay", '\Mingos\uMacro\Macro\Vimeo')
		;

		$input = "Hey, watch this {{{iSleepAllNight?id=lumberjack}}} video. And this {{{iWorkAllDay?id=lumberjack}}} video too!";
		$output = $parser->replace($input);

		$this->assertEquals($this->expectedOutput, $output);
	}

	/**
	 * If a correctly formed macro string is found, but no matching macro exists, an exception must be thrown.
	 */
	public function testReplacementWithInexistentMacro()
	{
		$this->setExpectedException("InvalidArgumentException", 'Unrecognised macro "i_dont_exist".');

		$parser = new Parser(new ParseStr());
		$parser->replace("Parse this yo! {{{i_dont_exist?foo=bar}}}");
	}
}
